# this python script extracts eq6 output into a table for import to Excel or plotting otherwise.
# the name of the eq6 output file is specified on the command line,
# the output is a comma-delimited file with the same prefix with but the suffix ".csv"

import sys

# some command-line error handling and hopefully more useful error messages
if len(sys.argv) < 2:
    print(
        """specify the EQ6 output filename on the command line
like this: "python extract_6o.py my_eq6_output.6o" """
    )
    sys.exit(1)
else:
    eq6outfn = sys.argv[1]

try:
    # EQ3/6 uses degree character, which isn't ASCII or utf-8
    fh = open(eq6outfn, "r", encoding="ISO-8859-1")
except IOError:
    print(f'cannot find file "{eq6outfn}"')
    sys.exit(1)

lines = fh.readlines()
fh.close()

DEBUG = False  # for printing some debugging info related to reading blocks from file

# enable selectively turning off high-level sections of EQ6 output file
# these will not be read, and will not be in csv output file.

sections = [
    "ph| --- The pH, Eh, pe-, and Ah on various pH scales ---",
    "el| --- Elemental Composition of the Aqueous Solution ---",
    "num|--- Numerical Composition of the Aqueous Solution ---",
    "sen|--- Sensible Composition of the Aqueous Solution ---",
    "aq| --- Distribution of Aqueous Solute Species ---",
    "rdx|--- Aqueous Redox Reactions ---",
    "sol|--- Summary of Solid Phases (ES) ---",
    "sat|--- Saturation States of Pure Solids ---",
    "fug|--- Fugacities ---",
    "iha|--- Ion-H+ Activity Ratios ---",
]

# turn on or off certain sections of EQ6 output to speed up and reduce the size of the output file
block_flags = []
NUM_SECTIONS = len(sections)
try:
    with open("options.txt", "r", encoding="ascii") as fh:
        for j in range(NUM_SECTIONS):
            line = fh.readline()
            if line.lstrip()[0].upper() == "T":
                block_flags.append(True)
            else:
                block_flags.append(False)

    print("found options.txt file with following sections turned on:")
    for s, bf in zip(sections, block_flags):
        if bf:
            print(s)
except IOError:
    # if no options file, read everything by default
    block_flags = [True] * NUM_SECTIONS

    # write an options file, that can be modified by user
    with open("options.txt", "w", encoding="ascii") as fho:
        for s in sections:
            fho.write(f"T {s}\n")
    print(
        """No options.txt file was found, default file was written.
Set first character of lines in this file to 'F' to
disable reading parts of EQ6 output."""
    )
except IndexError:
    print(
        """found options.txt file with incorrect number of rows.
Delete options.txt and try again."""
    )
    sys.exit(1)

if DEBUG:
    print("block_flags:", block_flags)

d = {}

# determine how many blocks of output there are
block_count = 0

for j, line in enumerate(lines):
    # find the row corresponding to the beginning of this block of output
    if "                    Xi=" in line:
        xi = float(line.split("=")[1])
        d[block_count] = {"row0": j}
        d[block_count]["Xi"] = xi

        # find the row corresponding to the end of this block of output
        for k, nline in enumerate(lines[j + 1 :]):
            next_line = k + j

            if "                    Xi=" in nline:
                break

        d[block_count]["row1"] = next_line

        if (
            " Fluid-centered flow-through open system print\n"
            in lines[d[block_count]["row0"] : d[block_count]["row1"]]
        ):
            if DEBUG:
                # this only come up with in "flow-through open system" mode
                # where there will be a short block (~50 rows) with minimal output labeled:
                # " Fluid-centered flow-through open system print following a discontinuity."
                print("FOUND A SHORT BLOCK, skipping")
            del d[block_count]
        else:
            block_count += 1

# make a list of all the output cut into blocks
output_blocks = []
for j, k in enumerate(sorted(d.keys())):
    output_blocks.append(lines[d[k]["row0"] : d[k]["row1"]])
    if DEBUG:
        print("block:", j, k, d[k]["row0"], d[k]["row1"] - d[k]["row0"])

# cycle through each block, reading data into python dictionaries
for b, _ in enumerate(output_blocks):
    for k, line in enumerate(output_blocks[b]):
        if " Temperature=" in line:
            d[b]["Temp_C"] = float(line.split()[1].strip())

        elif " Pressure=" in line:
            d[b]["Press_bars"] = float(line.split()[1].strip())

        elif (
            block_flags[0]
            and "--- The pH, Eh, pe-, and Ah on various pH scales ---" in line
        ):
            ph = {}
            if b == 0:
                d[0]["ph_cols"] = [
                    x.strip().replace(" ", "-").replace(",", "")
                    for x in output_blocks[b][k + 2].strip().split("  ")
                    if len(x) > 0
                ]

            for c, ll in enumerate(output_blocks[b][k + 4 :]):
                f = [x.strip() for x in ll.strip().split("  ") if len(x) > 0]
                if len(f) >= 2:
                    name = f[0].replace(" scale", "").replace(" ", "-")
                    ph[name] = [float(x) for x in f[1:]]
                else:
                    break
            d[b]["ph"] = ph

        elif "Activity of water=" in line:
            d[b]["aw"] = float(line.split("=")[1].strip())

        elif "Mole fraction of water=" in line:
            d[b]["Xw"] = float(line.split("=")[1].strip())

        elif "Activity coefficient of water=" in line:
            d[b]["gammaw"] = float(line.split("=")[1].strip())

        elif "Ionic strength (I)=" in line:
            d[b]["I_molal"] = float(line.split("=")[1].split()[0].strip())

        elif "Stoichiometric ionic strength=" in line:
            d[b]["StI_molal"] = float(line.split("=")[1].split()[0].strip())

        elif "Ionic asymmetry (J)=" in line:
            d[b]["J_molal"] = float(line.split("=")[1].split()[0].strip())

        elif "Stoichiometric ionic asymmetry=" in line:
            d[b]["StJ_molal"] = float(line.split("=")[1].split()[0].strip())

        # TODO: handle units of TDS and alkalinity more generally

        elif "Total dissolved solutes (TDS)=" in line:
            if "mg/L" in output_blocks[b][k + 1]:
                d[b]["TDS_mg/L"] = float(
                    output_blocks[b][k + 1].split("=")[1].split()[0]
                )
            elif "mg/kg.sol" in line:
                d[b]["TDS_mg/kg.sol"] = float(line.split("=")[1].split()[0])

        elif "Solution density=" in line and "g/L" in line:
            d[b]["rho_g/L"] = float(line.split("=")[1].split()[0])

        elif "Molarity/molality=" in line:
            d[b]["molar/molal_kg.H2O/L"] = float(line.split("=")[1].split()[0])

        elif "--- HCO3-CO3-OH Total Alkalinity ---" in line:
            if "mg/L" in output_blocks[b][k + 6]:
                d[b]["CarbAlk_mg/L-CaCO3"] = float(
                    output_blocks[b][k + 6].split("mg/L")[0]
                )
            elif "mg/kg.sol" in output_blocks[b][k + 3]:
                d[b]["CarbAlk_mg/kg.sol-CaCO3"] = float(
                    output_blocks[b][k + 3].split("mg/kg.sol")[0]
                )

        elif "--- Extended Total Alkalinity ---" in line:
            if "mg/L" in output_blocks[b][k + 6]:
                d[b]["ExtAlk_mg/L-CaCO3"] = float(
                    output_blocks[b][k + 6].split("mg/L")[0]
                )
            elif "mg/kg.sol" in output_blocks[b][k + 3]:
                d[b]["ExtAlk_mg/kg.sol-CaCO3"] = float(
                    output_blocks[b][k + 3].split("mg/kg.sol")[0]
                )

        elif "Actual Charge imbalance=" in line and "eq/kg.sol" in line:
            d[b]["act_charge_imb_eq/kg.sol"] = float(line.split("=")[1].split()[0])

        elif "Actual Charge imbalance=" in line and "eq" in line:
            d[b]["act_charge_imb_eq"] = float(line.split("=")[1].split()[0])

        elif "Relative charge discrepancy=" in line:
            d[b]["rel_charge_discr"] = float(line.split("=")[1])

        elif (
            block_flags[1]
            and "--- Elemental Composition of the Aqueous Solution ---" in line
        ):
            elemental = {}
            if b == 0:
                # column headers sometimes have single spaces in them, but should
                # have multiple spaces between columns.
                # replace spaces in column header names with dashes
                # first column header is "Element" (names of rows), which we
                # don't want in the list
                d[0]["elemental_cols"] = [
                    x.strip().replace(" ", "-")
                    for x in output_blocks[b][k + 2].strip().split("  ")
                    if len(x) > 0
                ][1:]

            for c, ll in enumerate(output_blocks[b][k + 4 :]):
                f = ll.split()
                if len(f) >= 2:
                    name = f[0]
                    elemental[name] = [float(x) for x in f[1:]]
                else:
                    break
            d[b]["elemental"] = elemental

        elif (
            block_flags[2]
            and "--- Numerical Composition of the Aqueous Solution ---" in line
        ):
            numerical = {}
            if b == 0:
                d[0]["numerical_cols"] = [
                    x.strip().replace(" ", "-")
                    for x in output_blocks[b][k + 2].strip().split("  ")
                    if len(x) > 0
                ][1:]

            for c, ll in enumerate(output_blocks[b][k + 4 :]):
                f = ll.split()
                if len(f) >= 2:
                    name = f[0].replace("_", "-")
                    numerical[name] = [float(x) for x in f[1:]]
                else:
                    break
            d[b]["numerical"] = numerical

        elif (
            block_flags[3]
            and "--- Sensible Composition of the Aqueous Solution ---" in line
        ):
            sensible = {}
            if b == 0:
                d[0]["sensible_cols"] = [
                    x.strip().replace(" ", "-")
                    for x in output_blocks[b][k + 2].strip().split("  ")
                    if len(x) > 0
                ][1:]

            for c, ll in enumerate(output_blocks[b][k + 4 :]):
                f = ll.split()
                if len(f) >= 2:
                    name = f[0].replace("_", "-")
                    sensible[name] = [float(x) for x in f[1:]]
                else:
                    break
            d[b]["sensible"] = sensible

        elif (
            block_flags[4] and "--- Distribution of Aqueous Solute Species ---" in line
        ):
            aqueous = {}
            if b == 0:
                d[0]["aqueous_cols"] = [
                    x.strip().replace(" ", "-")
                    for x in output_blocks[b][k + 2].strip().split("  ")
                    if len(x) > 0
                ][1:]

            for ll in output_blocks[b][k + 4 :]:
                f = ll.split()
                if len(f) >= 2:
                    name = f[0].replace("_", "-")
                    aqueous[name] = [float(x) for x in f[1:]]
                else:
                    break
            d[b]["aqueous"] = aqueous

        elif block_flags[5] and "--- Aqueous Redox Reactions ---" in line:
            redox = {}
            if b == 0:
                d[0]["redox_cols"] = [
                    x.strip().replace(" ", "-").replace(",", "")
                    for x in output_blocks[b][k + 2].strip().split("  ")
                    if len(x) > 0
                ][1:]

            for ll in output_blocks[b][k + 4 :]:
                f = ll.split()
                if len(f) >= 2:
                    name = f[0].replace("_", "-")
                    redox[name] = [float(x) for x in f[1:5]]
                else:
                    break
            d[b]["redox"] = redox

        elif block_flags[6] and "--- Summary of Solid Phases (ES) ---" in line:
            solids = {}
            if b == 0:
                d[0]["solids_cols"] = [
                    x.strip().replace(" ", "-").replace(",", "")
                    for x in output_blocks[b][k + 2].strip().split("  ")
                    if len(x) > 0
                ][1:]

            for ll in output_blocks[b][k + 4 :]:
                f = ll.split()
                if len(f) >= 2:
                    name = f[0].replace("_", "-")
                    solids[name] = [float(x) for x in f[1:5]]
                else:
                    break
            # need to handle "created, destroyed, net"?
            d[b]["solids"] = solids

        elif block_flags[7] and "--- Saturation States of Pure Solids ---" in line:
            saturation = {}
            if b == 0:
                d[0]["saturation_cols"] = [
                    x.strip().replace(" ", "-").replace(",", "")
                    for x in output_blocks[b][k + 2].strip().split("  ")
                    if len(x) > 0
                ][1:]

            # sometimes SATD/SSATD to right
            for ll in output_blocks[b][k + 4 :]:
                # this should handle mineral names with single spaces in them
                f = [x.strip() for x in ll.strip().split("  ") if len(x) > 0]
                if len(f) >= 2:
                    name = f[0].replace("_", " ")
                    saturation[name] = [float(x) for x in f[1:3]]
                else:
                    break
            d[b]["saturation"] = saturation

        elif block_flags[8] and "--- Fugacities ---" in line:
            fugacities = {}
            if b == 0:
                d[0]["fugacities_cols"] = [
                    x.strip().replace(" ", "-")
                    for x in output_blocks[b][k + 2].strip().split("  ")
                    if len(x) > 0
                ][1:]

            for ll in output_blocks[b][k + 4 :]:
                f = ll.split()
                if len(f) >= 2:
                    name = f[0].replace("_", " ")
                    fugacities[name] = []
                    for x in f[1:3]:
                        try:
                            v = float(x)
                        except ValueError:
                            if "-" in x[1:] or "+" in x[1:]:
                                # deal with annoying fortran trick of dropping the
                                # "E" to conserve space when 3 digits of exponent
                                v = x[0] + x[1:].replace("+", "E+").replace("-", "E-")
                                v = float(v)
                        fugacities[name].append(v)
                else:
                    break
            d[b]["fugacities"] = fugacities

        elif block_flags[9] and "--- Ion-H+ Activity Ratios ---" in line:
            ionHactivityRatio = {}
            for ll in output_blocks[b][k + 2 :]:
                f = [x.strip() for x in ll.split("=")]
                if len(f) >= 2:
                    # get entry at "Log ( a(***) ..."
                    name = f[0].split("a(")[1].split(")")[0]
                    ionHactivityRatio[name] = [
                        float(f[1]),
                    ]
                else:
                    break
            d[b]["ionHactivityRatio"] = ionHactivityRatio
            if b == 0:
                d[0]["ionHactivityRatio_cols"] = ["Log-Ion-H-activity-ratio"]

print(f"finished reading EQ6 output file '{eq6outfn}'")
# determine union of all columns across entire simulation

# simple blocks to output, and in what order (same every step?)
possible_keys = [
    "Xi",
    "Temp_C",
    "Press_bars",
    "aw",
    "Xw",
    "gammaw",
    "I_molal",
    "StI_molal",
    "J_molal",
    "StJ_molal",
    "TDS_mg/L",
    "TDS_mg/kg.sol",
    "molar/molal_kg.H2O/L",
    "rho_g/L",
    "CarbAlk_mg/L-CaCO3",
    "CarbAlk_mg/kg.sol-CaCO3",
    "ExtAlk_mg/L-CaCO3",
    "ExtAlk_mg/kg.sol-CaCO3",
    "act_charge_imb_eq/kg.sol",
    "act_charge_imb_eq",
    "rel_charge_discr",
]
lpk = len(possible_keys)
simple_keys = []

# don't include things from possible_keys in simple_keys,
# unless they were found in the datafile
for dblock in d:
    if lpk == len(simple_keys):
        break
    for pk in possible_keys:
        if pk in d[dblock] and pk not in simple_keys:
            simple_keys.append(pk)

if DEBUG:
    print("possible_keys", len(possible_keys), possible_keys)
    print("simple_keys", len(simple_keys), simple_keys)

# group blocks, columns depend on union of species/etc across all steps
possible_group_keys = [
    "elemental",
    "sensible",
    "aqueous",
    "redox",
    "ph",
    "numerical",
    "solids",
    "saturation",
    "fugacities",
    "ionHactivityRatio",
]

group_key_header_prefix = {
    "elemental": "el",
    "sensible": "sen",
    "aqueous": "aq",
    "redox": "rdx",
    "solids": "sol",
    "saturation": "sat",
    "numerical": "num",
    "fugacities": "fug",
    "ionHactivityRatio": "iha",
    "ph": "ph",
}

# leave out possible keys that were turned off manually (options.txt)
group_keys = []
for pgk, bflag in zip(possible_group_keys, block_flags):
    if bflag and pgk in d[0]:
        group_keys.append(pgk)

if DEBUG:
    print("possible_groups", len(possible_group_keys), possible_group_keys)
    print("selected_group", len(group_keys), group_keys)

group_headings = {}
group_cols = {}
for k in group_keys:
    set_group_headings = set()
    group_cols[k] = d[0][k + "_cols"]
    for j in range(len(output_blocks)):
        for val in d[j][k].keys():
            set_group_headings.add(val)
    group_headings[k] = sorted(list(set_group_headings))


# write output into giant table
csvfn = eq6outfn.replace(".6o", ".csv")
with open(csvfn, "w", encoding="ascii") as fh:

    # build up header
    headers = [f"g|{x}" for x in simple_keys[:]]
    for gk in group_keys:
        for gc in group_cols[gk]:
            for gh in group_headings[gk]:
                headers.append(f"{group_key_header_prefix[gk]}|{gh}_{gc}")

    print(f"writing csv summary file '{csvfn}'")
    # write header
    fh.write(",".join(headers) + "\n")

    # write rows of output
    for j in range(len(output_blocks)):
        out = []
        for sk in simple_keys:
            val = d[j][sk]
            if val == 0.0:
                out.append("0")  # unformatted number, to maybe allow telling them apart?
            else:
                out.append(f"{val:.6E}")
        for gk in group_keys:
            for k, gc in enumerate(group_cols[gk]):
                for gh in group_headings[gk]:
                    if gh in d[j][gk]:
                        val = d[j][gk][gh][k]
                        if val == 0.0:
                            out.append("0")
                        else:
                            out.append(f"{val:.6E}")
                    else:
                        out.append("")
        fh.write(",".join(out) + "\n")

