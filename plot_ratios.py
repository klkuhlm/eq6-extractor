import numpy as np
import matplotlib

matplotlib.use("Agg")
import matplotlib.pyplot as plt

# https://github.com/Phlya/adjustText
from adjustText import adjust_text

fn = "SW-25b.csv"
fh = open(fn, "r")
headers = fh.readline().split(",")
fh.close()

d = np.genfromtxt(fn, skip_header=1, delimiter=",", missing_values=np.NaN)  # data

idx = {"SO4--": -999, "Na+": -999, "Cl-": -999, "K+": -999, "Mg++": -999, "aw": -999}

# find which column each of the species is
for j, f in enumerate(headers):
    if "sen|" in f:
        if "mg/L" in f:
            for sp in idx.keys():
                if sp in f:
                    idx[sp] = j
    elif "g|" in f:
        if "aw" in f:
            idx["aw"] = j


aw_cutoff = 0.425
idx_max = d.shape[0]
for j in range(d.shape[0]):
    if d[j, idx["aw"]] < aw_cutoff:
        idx_max = j - 1
        break

print("columns in data:", idx)

# plot data on figure
fig = plt.figure(1, figsize=(11, 8))
ax = []
ax.append(fig.add_subplot(221))
ax.append(fig.add_subplot(222))
ax.append(fig.add_subplot(223))
ax.append(fig.add_subplot(224))

NaCl_lim = [0.0, 0.5]
KMg_lim = [0.0, 0.9]
SO4_lim = [0, 40000]

ms = 1.0
lw = 0.25
dot_color = "red"
line_color = "gray"

# main plot
KMg = d[:idx_max, idx["K+"]] / d[:idx_max, idx["Mg++"]]
NaCl = d[:idx_max, idx["Na+"]] / d[:idx_max, idx["Cl-"]]
ax[0].plot(KMg, NaCl, ".", color=dot_color, markersize=ms)
ax[0].plot(KMg, NaCl, "-", color=line_color, linewidth=lw)
ax[0].set_xlabel("K$^+$/Mg$^{++}$ (weight ratio)")
ax[0].set_ylabel("Na$^+$/Cl$^-$ (weight ratio)")
ax[0].set_xlim(KMg_lim)
ax[0].set_ylim(NaCl_lim)
ax[0].grid()

# share y-axis
ax[1].plot(d[:idx_max, idx["SO4--"]], NaCl, ".", color=dot_color, markersize=ms)
ax[1].plot(d[:idx_max, idx["SO4--"]], NaCl, "-", color=line_color, linewidth=lw)
ax[1].set_xlabel("SO$_4^{--}$ (mg/L)")
ax[1].set_ylabel("Na$^+$/Cl$^-$ (weight ratio)")
ax[1].set_xlim(SO4_lim)
ax[1].set_ylim(NaCl_lim)
ax[1].grid()

# share x-axis
ax[2].plot(KMg, d[:idx_max, idx["SO4--"]], ".", color=dot_color, markersize=ms)
ax[2].plot(KMg, d[:idx_max, idx["SO4--"]], "-", color=line_color, linewidth=lw)
ax[2].set_ylabel("SO$_4^{--}$ (mg/L)")
ax[2].set_xlabel("K$^+$/Mg$^{++}$ (weight ratio)")
ax[2].set_xlim(KMg_lim)
ax[2].set_ylim(SO4_lim)
ax[2].grid()

# species vs. activity of water
for sp in idx.keys():
    if "aw" not in sp:
        ax[3].plot(d[:idx_max, idx["aw"]], d[:idx_max, idx[sp]], "-", label=sp)
ax[3].set_yscale("log")
ax[3].set_xlabel("$a_w$")
ax[3].set_ylabel("Sensible composition (mg/L)")
ax[3].set_xlim([0.9, 0.3])
# ax[3].set_ylim(NaCl_lim)
# ax[3].grid()
ax[3].legend(loc=0, fontsize="small")

minerals = {
    "Bischofite": "$MgCl_2\\cdot 6H_2O$",
    "Carnallite": "$KMgCl_3\\cdot 6H_2O$",
    "Kainite": "$KMgSO_4Cl\\cdot3 H_2O$",
    "Halite": "$NaCl$",
    "Kieserite": "$MgSO_4\\cdot H_2O$",
    "Leonite": "$K_2Mg(SO_4)_2\\cdot 4H_2O$",
    "Sylvite": "$KCl$",
    "Oxychloride-Ca": "$CaCl_2(CaO)_3\\cdot 16H_2O$",
    "Boric-acid": "$B(OH)_3$",
    "Anhydrite": "$CaSO_4$",
    "Polyhalite": "$K_2Ca_2Mg(SO_4)_4\\cdot 2H_2O$",
    "Magnesite": "$MgCO_3$",
    "Glaserite": "$(K,Na)_3Na(SO_4)_2$",
    "Glauberite": "$Na_2 Ca(SO_4)_2$",
    "KB5O8:4H2O": "$KB_5O_8\\cdot 4H_2O$",
}

solid_moles_threshold = 1.0e-6
anns = [[], [], []]
lfs = 6

for j, f in enumerate(headers):
    if "sol|" in f:
        if "Moles" in f and "fix-" not in f:
            min_name = f.split("_")[0].split("|")[1]
            if min_name in minerals:
                label = minerals[min_name]
            else:
                label = min_name
            for i in range(idx_max):
                if np.isfinite(d[i, j]):
                    if d[i, j] > solid_moles_threshold:
                        c1 = [
                            d[i, idx["K+"]] / d[i, idx["Mg++"]],
                            d[i, idx["Na+"]] / d[i, idx["Cl-"]],
                        ]
                        ax[0].plot(c1[0], c1[1], "x")
                        anns[0].append(ax[0].text(c1[0], c1[1], label, fontsize=lfs))

                        c2 = [d[i, idx["SO4--"]], c1[1]]
                        ax[1].plot(c2[0], c2[1], "x")
                        anns[1].append(ax[1].text(c2[0], c2[1], label, fontsize=lfs))

                        c3 = [c1[0], c2[0]]
                        ax[2].plot(c3[0], c3[1], "x")
                        anns[2].append(ax[2].text(c3[0], c3[1], label, fontsize=lfs))

                        aw = d[i, idx["aw"]]
                        y = 1.0e4
                        ax[3].axvline(d[i, idx["aw"]], color="gray", linewidth=0.25)
                        ax[3].text(aw, y, label, fontsize=lfs, rotation="vertical")
                        break

for j, (x, n) in enumerate(zip(ax, anns)):
    adjust_text(
        n,
        ax=x,
        force_text=0.05,
        expand_points=(1.5, 1.5),
        arrowprops=dict(arrowstyle="-", color="gray", linewidth=0.5),
    )

plt.tight_layout()
plt.suptitle(fn.split(".")[0])
plt.savefig(fn.replace(".csv", "_ratio.png"))
