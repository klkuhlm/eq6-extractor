This script creates a comma-separated text file (csv), based on the output of EQ6.  The reaction progress corresponds to the rows, and there are columns for the major species or other properties being computed at each step.

Usage:

python extract_6o.py my_eq6_output.6o

You can specify to turn off some sections of the output (which will speed up the reading, and make the output file smaller)
by changing the "T" on the first line of the options.txt file to "F" for the sections of interest. 

The script will generate an options.txt file, if you run it and it doesn't find one.

The major benefit of this script over the excel macro that comes with EQ6 is that this script reads in all the text output of EQ6 before starting to write the .csv file, so it can accomodate species that come and go during the reaction. In the excel macro, only species that exist at the beginning of the reaction can be output, without specifying them explicity. 

This was developed for evaporation experiments, where water is removed and the mineral species that precipitate are monitored as the activity of water decreases. In our case, there were no or very few mineral species at the begining of the experiment, so I developed this tool to better analyze models of evaporation experiments.

The script does not output everything EQ6 reports (which is a lot of stuff and sometimes the formatting or column structure used by EQ6 is rather baroque), but I have found it pretty easy to extend the script multiple times to output more or different variables of interest. The approach is quite general.